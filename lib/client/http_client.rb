
require 'net/http'
require_relative 'client'

class HttpClient < Client
    def checkStatus
        return get
    end
    
    private
    def get
        begin
            response = changeReponse(Net::HTTP.get_response(URI(@url)))
        rescue
            response = changeReponse(Net::HTTPError)
        end
        response
    end
    
    def changeReponse(httpStatus)
        case httpStatus
        when Net::HTTPSuccess
            return ClientRep::Success
        else
            return ClientRep::Error
        end
    end
end