module ClientRep
  Success = 0
  Error = 1
end



class Client
  attr_accessor :url
  DEFAULT_URL = "https://about.gitlab.com"
  
  def initialize(url=DEFAULT_URL)
    @url = url
  end

  def checkStatus
    raise NotImplementedError
  end
  
end



