
require 'command/command'
require 'command/help_command'
require 'command/status_command'
require 'command/unsupport_command'
require 'client/http_client'

class CLICommandFactory
    def self.getCommand(opts={})
        command = nil
        case opts[:command]
        when CLICommand::CHECK_STATUS then command = getCheckStatusCmd
        when CLICommand::HELP then command = getHelpCmd
        else
            command = getUnSupportCmd
        end    
        command
    end

    private
    
    def self.getCheckStatusCmd
        StatusCommand.new HttpClient.new
    end
    
    def self.getHelpCmd
        HelpCommand.new HttpClient.new
    end
    
    def self.getUnSupportCmd
        UnSupportCommand.new HttpClient.new
    end
  
end