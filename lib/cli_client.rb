require 'command/command_parser'
require 'cli_command_factory'

class CLIClient
    def self.executeCmd(args=[])
        opts = CommandParser.parse args
        cmd = CLICommandFactory.getCommand opts
        cmd.execute
    end
end