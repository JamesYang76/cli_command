
require_relative 'command'

class HelpCommand < Command
    include Usage
    def execute
        puts "\n##### Help #####"
        puts "Default Access URL :" + Client::DEFAULT_URL
        puts "\n"
        print_command_list
    end
end
