require_relative 'command'
require 'concurrent'

class StatusCommand < Command
    attr_accessor  :execution_interval, :execution_time
    def execution_interval
        @execution_interval ||= 10
    end
  
    def execution_time
        @execution_time ||= 60
    end
      
    def execute
        count = 1
        puts "\nAccess URL: " + client.url
        execution_times = Array.new 
        task = Concurrent::TimerTask.new(run_now: true,execution_interval: self.execution_interval) do
           start = Time.now        
           res = client.checkStatus
           response_time = Time.now - start
           
           if(ClientRep::Success == res)
               execution_times << response_time
               puts "Count = #{count} /  Response time = #{response_time}s"
           else
               puts "Count = #{count} / Fail"
           end
           count += 1
        end
        
        tastExecuteWaitResponse(task)
        avg = getAvgResTime(execution_times)
        puts "\nAverage Response Time: #{avg}s" if not 0 == avg
    end
    
    
    private
    def tastExecuteWaitResponse(task)
        task.execute
        sleep execution_time
        task.shutdown
    end
    
    def getAvgResTime(execution_times)
        avg = 0
        sum = 0
        execution_times.each do |i|
            sum += i
        end
        avg = sum / execution_times.length if not 0 == execution_times.length 
        avg
    end
 
end
