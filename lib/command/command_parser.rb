require 'optparse'


class CommandParser
    def self.parse(args = [])
        options = {}
        parser = OptionParser.new do |opts|
            opts.banner = "Usage: cliCommannd [options]"
            opts.on('-c', '--command Command') { |cmd| options[:command] = cmd }
        end
        begin
            parser.parse!(args)
        rescue OptionParser::MissingArgument
        end
        options
    end
end
