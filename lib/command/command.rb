require 'client/http_client'

module CLICommand
  CHECK_STATUS  = "checkStatus"
  HELP          = "help"
end


class Command
  attr_reader :client

  def initialize(client = HttpClient.new)
    @client = client || HttpClient.new
  end

  def execute
    raise NotImplementedError
  end
end


module Usage
  def print_command_list
      puts "Usage: cli_client -c Command"
      puts "\n"
      puts "Command List:"
      CLICommand.constants.each do |cmd|
         puts "\t" + CLICommand.const_get(cmd)
      end
  end
end