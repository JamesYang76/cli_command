


Gem::Specification.new do |s|
  s.name        = 'cli_client'
  s.version     = '0.0.3'
  s.platform    = Gem::Platform::RUBY
  s.executables << 'cli_client'
  s.date        = '2018-02-19'
  s.summary     = "CLI Command"
  s.description = "A simple CLI command gem"
  s.authors     = ["James Yang"]
  s.email       = 'chandleryang76@gmail.com'
  s.files       = Dir.glob("{lib}/**/*.rb")
  s.license     = 'MIT'
end