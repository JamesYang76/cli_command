require 'minitest/autorun'
require 'client/client'
require 'client/http_client'

class TestHttpClient < Minitest::Unit::TestCase
  
  def setup
    @testUrl = "https://about.gitlab.com"
    @httpClient = HttpClient.new @testUrl
  end
  
  def test_implements_url_attr
    assert_respond_to @httpClient, :url
  end
  
  def test_confirm_url_value
    assert_equal @testUrl, @httpClient.url
  end
  
  def test_check_status_with_success
    assert_equal  ClientRep::Success, @httpClient.checkStatus
  end
  
  def test_check_status_with_invalid_url
    @httpClient.url =  "https://g.com"
    assert_equal  ClientRep::Error, @httpClient.checkStatus
  end
  
end