require "minitest/autorun"


require 'command/status_command'
require 'client/http_client'


class TestStatusCommand< Minitest::Unit::TestCase
  def setup
      @status_cmd =  StatusCommand.new HttpClient.new "https://about.gitlab.com"
  end
  
  def test_implements_client_attr
      assert_respond_to @status_cmd, :client
  end
  
  def test_implements_execution_interval_attr
      assert_respond_to @status_cmd, :execution_interval
  end
  
  def test_confirm_execution_interval_value
      @status_cmd.execution_interval = 5
      assert_equal 5, @status_cmd.execution_interval
  end
  
  def test_implements_execution_time_attr
      assert_respond_to @status_cmd, :execution_time
  end
  
  def test_confirm_execution_time
      @status_cmd.execution_time = 20
      assert_equal 20, @status_cmd.execution_time
  end
  
  def test_implements_execute
      @status_cmd.execute
  end
  
  def test_implements_execute_with_invalid_url
      @status_cmd =  StatusCommand.new HttpClient.new "https://g.com"
      @status_cmd.execute
  end
  
end