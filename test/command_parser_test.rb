require "minitest/autorun"
require 'command/command'
require 'command/command_parser'

class TestCommandParser < Minitest::Unit::TestCase
 
  def test_check_status_cmd
    args = ["-c",CLICommand::CHECK_STATUS ]
    options = CommandParser.parse args
    assert_equal CLICommand::CHECK_STATUS, options[:command] 
  end
  
  
  def test_check_help_cmd
    args = ["-c", CLICommand::HELP]
    options = CommandParser.parse args
    assert_equal CLICommand::HELP, options[:command] 
  end
  
  def test_check_test_cmd
    args = ["-c", 'test']
    options = CommandParser.parse args
    assert_equal 'test', options[:command] 
  end
  
  def test_check_no_command
    args = ["-c"]
    no_args= {}
    options = CommandParser.parse args
    assert_equal no_args, options
  end
  
  def test_no_args
    no_args= {}
    options = CommandParser.parse
    assert_equal no_args, options
  end
    
end