require "minitest/autorun"
require 'cli_command_factory'


class TestCLICommandFactory< Minitest::Unit::TestCase
  
  def test_get_check_status_cmd
      opts = {:command => CLICommand::CHECK_STATUS}
      cmd = CLICommandFactory.getCommand opts
      assert_equal true, cmd.is_a?(StatusCommand)
  end
  
  def test_check_help_cmd
      opts = {:command => CLICommand::HELP }
      cmd = CLICommandFactory.getCommand opts
      assert_equal true, cmd.is_a?(HelpCommand)
  end
  
   def test_check_unsupport_cmd
       opts = {:command => 'getData'}
       cmd = CLICommandFactory.getCommand opts
       assert_equal true, cmd.is_a?(UnSupportCommand)
   end
   
   def test_check_no_cmd
       cmd = CLICommandFactory.getCommand 
       assert_equal true, cmd.is_a?(UnSupportCommand)
   end
  
end