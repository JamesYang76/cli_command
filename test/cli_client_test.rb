
require "minitest/autorun"
require 'cli_client'

class TestCLIClient< Minitest::Unit::TestCase
    
    def test_execute_help_cmd
        args = ["-c", CLICommand::HELP]
        CLIClient.executeCmd args
    end
    
    
    def test_execute_check_status_cmd
        args = ["-c",CLICommand::CHECK_STATUS]
        CLIClient.executeCmd args
    end
    
end