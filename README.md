### CLI Command

**gem build and install**
``` bash
 $gem build cli_client.gemsepc
 $gem install cli_client
```

**how to use**
``` bash
$./bin/cli_client -c checkStatus

Access URL: https://about.gitlab.com
Count = 1 /  Response time = 1.253251901s
Count = 2 /  Response time = 1.181913514s
Count = 3 /  Response time = 1.180218793s
Count = 4 /  Response time = 1.176959148s
Count = 5 /  Response time = 1.214687344s
Count = 6 /  Response time = 1.181048924s

Average Response Time: 1.1980132706666666s


$./bin/cli_client -c help

##### Help #####
Default Access URL :https://about.gitlab.com

Usage: cli_client -c Command

Command List:
        checkStatus
        help
```
**cli_client**
``` ruby
#!/usr/bin/env ruby
require 'cli_client'

CLIClient.executeCmd ARGV
```
**Unit Test**
``` bash
$rake test:all
$rake test:http_client
$rake test:status_command
$rake test:help_command
$rake test:unsupport_command
$rake test:parser
$rake test:command_factory
$rake test:cli_client
```